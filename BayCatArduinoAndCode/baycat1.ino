




#include <Servo.h>
#include <LiquidCrystal.h>



Servo servo; //define servo as Servo object
LiquidCrystal lcd(12, 11, 5, 4, 3, 2); //Digital pins to which you connect the LCD
const int inPin = 0; // A0 is where you connect the sensor


void setup()
{
  servo.attach(10); //define pin
  lcd.begin(16,2); //cols, rows
}



void loop()
{
 
  
  /*
  
  
  Things to be added:
  
  -Button or other method to indicate that measuring has been initiated
  
 
  -Led will light up ( This section should take artificial 5 seconds)
  
  -Servo will turn 180 degrees
  
  -> moves to rest of the code
  
  */
  
  int value = analogRead(inPin); //read the value from the sensor
  
  lcd.setCursor(0,1);
  float millivolts = (value / 1024.0) * 5000; 
  float celsius = millivolts / 10;
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(celsius);    //print calculated value
  lcd.print("C");
  
  delay(1000);
}